package com.exam.exammodule03.fragments;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class AnimationFragment extends BaseFragment{

    ImageView img, img2;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_animation;
    }

    @Override
    public void initView(View view) {
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);
        View btn4 = view.findViewById(R.id.button4);
        View btn5 = view.findViewById(R.id.button5);

        View btn6 = view.findViewById(R.id.button6);
        View btn7 = view.findViewById(R.id.button7);
        View btn8 = view.findViewById(R.id.button8);
        View btn9 = view.findViewById(R.id.button9);
        View btn10 = view.findViewById(R.id.button10);

        img = (ImageView) view.findViewById(R.id.imageView);
        img2 = (ImageView) view.findViewById(R.id.imageView2);

        btn1.setOnClickListener(onclick);
        btn2.setOnClickListener(onclick);
        btn3.setOnClickListener(onclick);
        btn4.setOnClickListener(onclick);
        btn5.setOnClickListener(onclick);

        btn6.setOnClickListener(onclick);
        btn7.setOnClickListener(onclick);
        btn8.setOnClickListener(onclick);
        btn9.setOnClickListener(onclick);
        btn10.setOnClickListener(onclick);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click");
            }
        });

    }

    View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button1:
                    alpha();
                    break;
                case R.id.button2:
                    scale();
                    break;
                case R.id.button3:
                    translate();
                    break;
                case R.id.button4:
                    rotate();
                    break;
                case R.id.button5:
                    scaleRotate();
                    break;

                case R.id.button6:
                    alpha2();
                    break;
                case R.id.button7:
                    scale2();
                    break;
                case R.id.button8:
                    translate2();
                    break;
                case R.id.button9:
                    rotate2();
                    break;
                case R.id.button10:
                    scaleRotate2();
                    break;

            }
        }
    };

    private void alpha(){
        AlphaAnimation alpha = (AlphaAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.alpha);

        /*img.setAnimation(alpha);
        //alpha.start();
        alpha.startNow();*/

        img.startAnimation(alpha);

    }

    private void scale(){
        ScaleAnimation scale = (ScaleAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.scale);
        img.startAnimation(scale);
    }

    private void translate(){
        TranslateAnimation scale = (TranslateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.translate);
        img.startAnimation(scale);
    }

    private void rotate(){
        RotateAnimation scale = (RotateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        img.startAnimation(scale);
    }

    private void scaleRotate(){
        AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(getActivity(), R.anim.rotatescale);
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img.startAnimation(set);
    }

    private void alpha2(){
        AlphaAnimation alpha = (AlphaAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.alpha2);

        /*img.setAnimation(alpha);
        //alpha.start();
        alpha.startNow();*/

        img2.startAnimation(alpha);

    }

    private void scale2(){
        ScaleAnimation scale = (ScaleAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.scale2);
        img2.startAnimation(scale);
    }

    private void translate2(){
        TranslateAnimation scale = (TranslateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.translate2);
        img2.startAnimation(scale);
    }

    private void rotate2(){
        RotateAnimation scale = (RotateAnimation) AnimationUtils.loadAnimation(getActivity(), R.anim.rotate2);
        img2.startAnimation(scale);
    }

    private void scaleRotate2(){
        AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(getActivity(), R.anim.rotatescale2);
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img2.startAnimation(set);
    }
}
