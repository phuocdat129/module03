package com.exam.exammodule03;

import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.exam.exammodule03.constanst.FragmentId;
import com.exam.exammodule03.fragments.Animation2Fragment;
import com.exam.exammodule03.fragments.AnimationFragment;
import com.exam.exammodule03.fragments.BaseFragment;
import com.exam.exammodule03.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        addFragment(FragmentId.HOME, false);

    }

    public void addFragment(FragmentId id, boolean addToStack){

        BaseFragment fragment = null;
        if(id == FragmentId.HOME){
            fragment = new HomeFragment();
        }
        else if(id==FragmentId.ANIMATION){
            fragment = new AnimationFragment();
        }
        else if(id==FragmentId.ANIMATION2){
            fragment = new Animation2Fragment();
        }

        FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ts = manager.beginTransaction();
        /*ts.setCustomAnimations(R.animator.slide_in_right,
                R.animator.slide_out_left,
                R.animator.slide_in_left,
                R.animator.slide_out_right);*/

        ts.setCustomAnimations(R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right);

        ts.replace(R.id.contain, fragment);
        if(addToStack){
            ts.addToBackStack(id.getKey());
        }

        //
        ts.commit();

    }

}