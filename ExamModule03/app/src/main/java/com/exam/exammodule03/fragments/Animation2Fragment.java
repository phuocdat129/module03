package com.exam.exammodule03.fragments;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class Animation2Fragment extends BaseFragment{

    ImageView img, img2, img3, img4;

    @Override
    public int getFragmentId() {
        return R.layout.fragment_animation2;
    }

    @Override
    public void initView(View view) {
        View btn1 = view.findViewById(R.id.button1);
        View btn2 = view.findViewById(R.id.button2);
        View btn3 = view.findViewById(R.id.button3);
        View btn4 = view.findViewById(R.id.button4);
        View btn5 = view.findViewById(R.id.button5);
        View btn6 = view.findViewById(R.id.button6);
        View btn7 = view.findViewById(R.id.button7);
        View btn8 = view.findViewById(R.id.button8);
        View btn9 = view.findViewById(R.id.button9);
        View btn10 = view.findViewById(R.id.button10);

        img = (ImageView) view.findViewById(R.id.imageView);
        img2 = (ImageView) view.findViewById(R.id.imageView2);
        img3 = (ImageView) view.findViewById(R.id.imageView3);
        img4 = (ImageView) view.findViewById(R.id.imageView4);

        btn1.setOnClickListener(onclick);
        btn2.setOnClickListener(onclick);
        btn3.setOnClickListener(onclick);
        btn4.setOnClickListener(onclick);
        btn5.setOnClickListener(onclick);
        btn6.setOnClickListener(onclick);
        btn7.setOnClickListener(onclick);
        btn8.setOnClickListener(onclick);
        btn9.setOnClickListener(onclick);
        btn10.setOnClickListener(onclick);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click");
            }
        });

        //start animation drawable
        AnimationDrawable anim = (AnimationDrawable) img2.getDrawable();
        anim.start();

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click");
            }
        });
        //start animation 2 drawable
        AnimationDrawable anim2 = (AnimationDrawable) img4.getDrawable();
        anim2.start();
    }


    View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button1:
                    alpha();
                    break;
                case R.id.button2:
                    scale();
                    break;
                case R.id.button3:
                    translate();
                    break;
                case R.id.button4:
                    rotate();
                    break;
                case R.id.button5:
                    scaleRotate();
                    break;

                case R.id.button6:
                    alpha2();
                    break;
                case R.id.button7:
                    scale2();
                    break;
                case R.id.button8:
                    translate2();
                    break;
                case R.id.button9:
                    rotate2();
                    break;
                case R.id.button10:
                    scaleRotate2();
                    break;

            }
        }
    };

    private void alpha(){
        ObjectAnimator alpha = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.alpha);
        alpha.setTarget(img);
        alpha.start();
    }

    private void scale(){
        AnimatorSet scale = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.scale);
        scale.setTarget(img);
        scale.start();
    }

    private void translate(){
        AnimatorSet translate = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate);
        translate.setTarget(img);
        translate.start();
    }

    private void rotate(){
        AnimatorSet rotate = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.rotation);
        rotate.setTarget(img);
        rotate.start();
    }

    private void scaleRotate(){
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate_rotate);
        set.setTarget(img);
        set.start();
    }

    private void alpha2(){
        ObjectAnimator alpha2 = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.alpha2);
        alpha2.setTarget(img3);
        alpha2.start();
    }
    private void scale2(){
        AnimatorSet scale2 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.scale2);
        scale2.setTarget(img3);
        scale2.start();
    }

    private void translate2(){
        AnimatorSet translate2 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate2);
        translate2.setTarget(img3);
        translate2.start();
    }

    private void rotate2(){
        AnimatorSet rotate2 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.rotation2);
        rotate2.setTarget(img3);
        rotate2.start();
    }

    private void scaleRotate2(){
        AnimatorSet set2 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.translate_rotate2);
        set2.setTarget(img3);
        set2.start();
    }
}
