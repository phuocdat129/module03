package com.exam.exammodule03.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exam.exammodule03.R;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public class HomeAdapter extends BaseAdapter{

    String[] data = new String[]{
            "pos 00: Bai 01 - Animation",
            "pos 01: Bai 01 - Animation (tt)",
            "pos 02: Bai 02 - SQLite",
            "pos 03: Bai 0 - Bai tiep theo",
            "pos 04: Bai 0 - Bai tiep theo",
            "pos 05: Bai 0 - Bai tiep theo",
            "pos 06: Bai 0 - Bai tiep theo",
            "pos 07: Bai 0 - Bai tiep theo",
            "pos 08: Bai 0 - Bai tiep theo",
            "pos 09: Bai 0 - Bai tiep theo",
            "pos 10: Bai 0 - Bai tiep theo",
            "pos 11: Bai 0 - Bai tiep theo",
            "pos 12: Bai 0 - Bai tiep theo",
            "pos 13: Bai 0 - Bai tiep theo",
            "pos 14: Bai 0 - Bai tiep theo",
            "pos 15: Bai 0 - Bai tiep theo",
            "pos 16: Bai 0 - Bai tiep theo",
            "pos 17: Bai 0 - Bai tiep theo",
            "pos 18: Bai 0 - Bai tiep theo",
            "pos 19: Bai 0 - Bai tiep theo",
            "pos 20: Bai 0 - Bai tiep theo",
            "pos 21: Bai 0 - Bai tiep theo",
            "pos 22: Bai 0 - Bai tiep theo",
            "pos 23: Bai 0 - Bai tiep theo",
            "pos 24: Bai 0 - Bai tiep theo",
            "pos 25: Bai 0 - Bai tiep theo",
            "pos 26: Bai 0 - Bai tiep theo",
            "pos 27: Bai 0 - Bai tiep theo",
            "pos 28: Bai 0 - Bai tiep theo",
            "pos 29: Bai 0 - Bai tiep theo",
            "pos 30: Bai ket thuc Module 03 - "
    };

    private Context context;

    public HomeAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        view = inflater.inflate(R.layout.layout_item_main, parent, false);

        TextView tv = (TextView) view.findViewById(R.id.nameTextView);
        tv.setText(getItem(position).toString()+" - Click xem");
        tv.setVisibility(View.VISIBLE);

        if(position%2==0){
            view.setBackgroundColor(Color.argb(80, 99, 22, 44));
        }
        else{
            view.setBackgroundColor(Color.argb(80, 11, 22, 99));
        }

        return view;
    }
}
