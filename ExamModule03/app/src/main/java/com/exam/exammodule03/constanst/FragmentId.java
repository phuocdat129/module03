package com.exam.exammodule03.constanst;

/**
 * Created by PC14-02 on 11/16/2015.
 */
public enum FragmentId {

    HOME("home"),
    ANIMATION("animation"),
    ANIMATION2("animation2");

    private String key;

    private FragmentId(String key){
        this.key = key;
    }

    public String getKey(){
        return key;
    }

}
